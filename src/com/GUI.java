package com;
import java.awt.*;

import org.jdatepicker.*;
import org.jdatepicker.impl.DateComponentFormatter;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.graphics.*;
import org.jdatepicker.impl.UtilDateModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Properties;

import javax.swing.*;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

//import org.jdatepicker.impl.JDatePickerImpl;

/*
 * TO JEST TO, KT�RE MA BYC!
 */
public class GUI extends JFrame{

	private JFrame frame;
	private JPanel panelExam, panelPatient, panelTable;
	private JTable listPatient;
	private JButton bSavePatient, bAbortPatient, bSaveExam, bAbortExam;
	private JLabel lApp, lZamknij, lPatientData, lName, lSurname, lPesel, lSex, lInsurance,  lExam, lDate,
			lErytrocythes, lHemoglobin, lIron;
	private JTextField tfName, tfSurname, tfPesel, tfErytrocythes, tfHemoglobin, tfIron;
	private JRadioButton radioMan, radioWoman;
	private JComboBox<String> listaInsurance;
	private JDatePicker data;
	private JTable patientTable;
	private Presenter presenter;

	private String[] tableColumns = {"imię i nazwisko", "płeć", "PESEL", "ubezpieczenie", "wynik"};

	private JScrollPane sp;
	private final int tfWidth = 15 ;

	private GUI()
    {
        start();
    }
	private void start() {

		presenter = new Presenter();


		frame = new JFrame ("Rejestracja Wyników Badań");
		frame.setSize(800, 600);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.setLayout(null);

		/////////////////////////////
		// wszystkie komponenty dodaje ostatecznie na koniec kodu
		////////////////////////////


		/////////PASEK MENU//////////////
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu menu = new JMenu("Aplikacja...");

		JMenuItem closeApp = new JMenuItem("zamknij (ALTF+4)");
		closeApp.addActionListener(source -> frame.dispose()); // na klikniecie zmaykamy ramke

		///////////////////////////////////////////////////////
		///////////////// PANEL PACJENTA///////////////////////



		//ustawianie paramentrow
		panelPatient = new JPanel();
		panelPatient.setLayout(new BoxLayout(panelPatient, BoxLayout.PAGE_AXIS));
		TitledBorder title;
		title = BorderFactory.createTitledBorder("Dane Pacjenta");
		panelPatient.setBorder(title);

		// komponenty//
		JPanel nameP = new JPanel();
		lName = new JLabel("Imię: ");
		lName.setFont(new Font("SansSerif", Font.ITALIC, 13));


		tfName = new JTextField();
		tfName.setFont(new Font("SansSerif", Font.ITALIC, 13));

		tfName.setColumns(tfWidth);
		nameP.add(lName); nameP.add(tfName);

		JPanel surnameP = new JPanel();
		lSurname = new JLabel("Nazwisko: ");
		lSurname.setFont(new Font("SansSerif", Font.ITALIC, 13));


		tfSurname = new JTextField();
		tfSurname.setFont(new Font("SansSerif", Font.ITALIC, 13));
		tfSurname.setColumns(tfWidth);
		surnameP.add(lSurname); surnameP.add(tfSurname);

		JPanel peselP = new JPanel();
		lPesel = new JLabel("PESEL: ");
		lPesel.setFont(new Font("SansSerif", Font.ITALIC, 13));

		tfPesel = new JTextField();
		tfPesel.setFont(new Font("SansSerif", Font.ITALIC, 13));
		tfPesel.setColumns(tfWidth);
		peselP.add(lPesel); peselP.add(tfPesel);

		JPanel sexP = new JPanel();
		lSex = new JLabel("Płeć: ");
		lSex.setFont(new Font("SansSerif", Font.ITALIC, 13));

		ButtonGroup group = new ButtonGroup();

		radioWoman = new JRadioButton("Kobieta");
		group.add(radioWoman);
		///////dodac akcje///////////
		//radioWoman.addActionListener(this);

		radioMan = new JRadioButton("Mężczyzna");
		group.add(radioMan);

		////////// dodac akcje ////////////
		//radioMan.addActionListener(this);
		sexP.add(radioWoman); sexP.add(radioMan);



		JPanel insuranceP = new JPanel();
		String[] lista = {"BRAK", "NFZ", "PRYWATNE"};
		lInsurance = new JLabel("Ubezpieczenie: ");
		lInsurance.setFont(new Font("SansSerif", Font.ITALIC, 13));


		listaInsurance = new JComboBox<>(lista);
		////// dodac akcje do listy//////
		insuranceP.add(lInsurance); insuranceP.add(listaInsurance);

		JPanel patientButtonP = new JPanel();
		bSavePatient = new JButton("Zapisz");
		bSavePatient.addActionListener((ActionEvent e) -> {
			int check = presenter.saveOrEditPatient(tfName.getText(), tfSurname.getText(), tfPesel.getText(),
					radioMan.isSelected(), String.valueOf(listaInsurance.getSelectedItem()), patientTable);

			 if (check==1){
				 JOptionPane.showMessageDialog(frame, "Wprowadzony PESEL musi być liczbą");
			 }
			 else if (check == 2) {
				 JOptionPane.showMessageDialog(frame, "Wprowadzony PESEL musi mieć 11 cyfr");
			 }
			 else if (check == 3) {
				 JOptionPane.showMessageDialog(frame, "Pacjent o tym numerze PESEL istnieje już w bazie  ");
			 }
			 else setPanelEditable(panelExam, true);
			 
		tfHemoglobin.setBackground(Color.WHITE);
		tfErytrocythes.setBackground(Color.WHITE);
		tfIron.setBackground(Color.WHITE);
		}
		);

		//bZapisz.addActionListener(zrodlo -> cosTuSieBedzieRobilo);
		///////AKCJA////////////

		bAbortPatient = new JButton("Anuluj");
		bAbortPatient.addActionListener((ActionEvent e) -> {
			clearFields(panelPatient);
			clearFields(panelExam);
			group.clearSelection();
			setPanelEditable(panelPatient, false);
			setPanelEditable(panelExam, false);
		});

		patientButtonP.add(bSavePatient); patientButtonP.add(bAbortPatient);


		//////////////////////////////////////////////////
		/////////////////PANEL BADANIE////////////////////
//		spaceY = 20; // zeby komponenty byly na dobrym Y, bo wczesniej caly czas zwieksza�am

		panelExam = new JPanel();
		panelExam.setLayout(new BoxLayout(panelExam, BoxLayout.PAGE_AXIS));
		TitledBorder tytul2;
		tytul2 = BorderFactory.createTitledBorder("Badanie");
		panelExam.setBorder(tytul2);
		setPanelEditable(panelExam, false);

		lDate = new JLabel("Data badania: ");
		lDate.setFont(new Font("SansSerif", Font.ITALIC, 13));
		//jdatepicker
		 //implementacja znaleziona tutaj
		// https://stackoverflow.com/questions/26794698/how-do-i-implement-jdatepicker
		 Properties p = new Properties();
		 UtilDateModel model = new UtilDateModel();
		 p.put("text.today", "Today");
		 p.put("text.month", "Month");
		 p.put("text.year", "Year");
        JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
        JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new  DateComponentFormatter());
		JPanel dateP = new JPanel();
		dateP.add(lDate);
		dateP.add(datePicker);


        lErytrocythes = new JLabel("Liczba erytrocytów (* 1012/l): ");
        lErytrocythes.setFont(new Font("SansSerif", Font.ITALIC, 13));
		tfErytrocythes = new JTextField();
		tfErytrocythes.setFont(new Font("SansSerif", Font.ITALIC, 13));
		tfErytrocythes.setColumns(tfWidth);
		JPanel erytP = new JPanel();
		erytP.add(lErytrocythes);
		erytP.add(tfErytrocythes);


		lHemoglobin = new JLabel("Stężenie hemoglobiny: (g/dl) "); // wyrazona integerem!!!
	    lHemoglobin.setFont(new Font("SansSerif", Font.ITALIC, 13));
	    //lHemoglobin.setBounds(spaceX, spaceY = spaceY+hLabel+3, wLabel, hLabel);
	 	tfHemoglobin = new JTextField();
		tfHemoglobin.setFont(new Font("SansSerif", Font.ITALIC, 13));
		tfHemoglobin.setColumns(tfWidth);
		JPanel hemoP = new JPanel();
		hemoP.add(lHemoglobin);
		hemoP.add(tfHemoglobin);


		lIron = new JLabel("Stężenie żelaza: (mmol/l) "); // wyrazona integerem!!!
	    lIron.setFont(new Font("SansSerif", Font.ITALIC, 13));
		tfIron = new JTextField();
		tfIron.setFont(new Font("SansSerif", Font.ITALIC, 13));
		tfIron.setColumns(tfWidth);
		JPanel ironP = new JPanel();
		ironP.add(lIron);
		ironP.add(tfIron);

		bSaveExam = new JButton("Zapisz");
		bSaveExam.addActionListener((ActionEvent e) ->{
					int check = presenter.saveExam((Date) datePicker.getModel().getValue() ,
					tfErytrocythes.getText(), tfHemoglobin.getText(), tfIron.getText(), patientTable, datePicker.getJFormattedTextField().getText());
					
					if(check == 1 ) {
						JOptionPane.showMessageDialog(frame, "Wprowadz kompletne dane");
					}
					else if(check == 2) {
						JOptionPane.showMessageDialog(frame, "Wprowadzone nieprawid�owe dane");
					}
					else {
						setPanelEditable(panelPatient, false);
						setPanelEditable(panelExam, false);
						clearFields(panelPatient);
						clearFields(panelExam);
						tfHemoglobin.setBackground(Color.WHITE);
						tfErytrocythes.setBackground(Color.WHITE);
						tfIron.setBackground(Color.WHITE);

					}
					});

		///////AKCJA////////////

		bAbortExam = new JButton("Anuluj");
		JPanel buttonsExamP = new JPanel();
		buttonsExamP.add(bSaveExam);
		buttonsExamP.add(bAbortExam);
		bAbortExam.addActionListener((ActionEvent e) ->{
			clearFields(panelExam);
		});




		////////////////////////////////////////
		////////////PRAWA TABELA///////////////
		panelTable = new JPanel();


		TitledBorder tytul3;
		tytul3 = BorderFactory.createTitledBorder("Tabela pacjentów");

		panelTable.setBorder(tytul3);
		panelTable.setLayout(new BoxLayout(panelTable, BoxLayout.PAGE_AXIS));

		patientTable = new JTable(){
			private static final long serialVersionUID = 1L;
			@Override
			public Class getColumnClass(int column) {
				switch (column) {
					case 4:
						return Boolean.class;
					default:
						return String.class;
				}
			}
		};
		DefaultTableModel defaultTableModel = new DefaultTableModel();
		defaultTableModel.setColumnIdentifiers(tableColumns);

		patientTable.setModel(defaultTableModel);
		patientTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		patientTable.getSelectionModel().addListSelectionListener(e ->{
			if (!patientTable.getSelectionModel().isSelectionEmpty()) {
				presenter.showPatient(patientTable, tfName, tfSurname, tfPesel, radioMan, radioWoman, listaInsurance);
				setPanelEditable(panelPatient, true);
				setPanelEditable(panelExam, true);

				if (presenter.vectorPatient.get(patientTable.getSelectedRow()).isExam()) {
					presenter.showExam(patientTable, datePicker, tfErytrocythes, tfHemoglobin, tfIron);
					if(!presenter.checkExamNormErytrocythes(patientTable) || !presenter.checkExamNormHemoglobin(patientTable) || !presenter.checkExamNormIron(patientTable))
					JOptionPane.showMessageDialog(frame, 
							"NORMY: \n"
							+ "erytrocyty: \n"
							+ "K 4,2 - 5,4 * 1012/l\n "
							+ "M 4,7 - 6,1 * 1012/l\n\n"
							+ "hemoglobina:\n"
							+ "K 11,5 - 16,0 g/dl\n"
							+ "M 12,5 - 18,0 g/dl\n\n"
							+ "żelazo:\n"
							+ "K 6,6 - 26 mmol/l\n"
							+ "M 10,6 - 28,3 mmol/l");
				}
				else {
					clearFields(panelExam);
					datePicker.getModel().setSelected(false);
					datePicker.getJFormattedTextField().setText("");
				}

			}

		});

		JScrollPane patientTableScroll = new JScrollPane(patientTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JButton bAddPatient = new JButton("Dodaj");
		bAddPatient.addActionListener( e ->{ setPanelEditable(panelPatient, true);
		 setPanelEditable(panelExam, false);
		 patientTable.clearSelection();
		 clearFields(panelPatient);
		 clearFields(panelExam);
		 tfHemoglobin.setBackground(Color.WHITE);
		 tfErytrocythes.setBackground(Color.WHITE);
		 tfIron.setBackground(Color.WHITE);
		} );

		JButton bRemovePatient = new JButton("Usuń");
		bRemovePatient.addActionListener((ActionEvent e)-> {
			if(!patientTable.getSelectionModel().isSelectionEmpty())
			{
				presenter.deletePatient(patientTable);
			}
			clearFields(panelPatient);
			clearFields(panelExam);
			setPanelEditable(panelExam, false);
			setPanelEditable(panelPatient, false);
			tfHemoglobin.setBackground(Color.WHITE);
			tfErytrocythes.setBackground(Color.WHITE);
			tfIron.setBackground(Color.WHITE);
		});



		JPanel tableButtonsP = new JPanel();
		tableButtonsP.setLayout(new BoxLayout(tableButtonsP, BoxLayout.LINE_AXIS));
		tableButtonsP.add(bAddPatient);
		tableButtonsP.add(bRemovePatient);

		panelTable.add(patientTableScroll);
		panelTable.add(tableButtonsP);



		//////////////DODAWANIE KOMPONENTOW//////////////////////////

		//menu//
		menuBar.add(menu);
		menu.add(closeApp);

		//panelPacjent//

		panelPatient.add(nameP);
		panelPatient.add(surnameP);
		panelPatient.add(peselP);
		panelPatient.add(sexP);
		panelPatient.add(insuranceP);
		panelPatient.add(patientButtonP);
		setPanelEditable(panelPatient, false);

		//-------------------------------------------------------------
		//------------------panelBadanie----------------------//---------
		//--------------------------------------------------------------
		//frame.add(panelExam);
		panelExam.add(dateP);
		panelExam.add(erytP);
		panelExam.add(hemoP);
		panelExam.add(ironP);
		panelExam.add(buttonsExamP);
		setPanelEditable(panelExam, false);


		//tabela pacjentow//
		//frame.add(panelTable);
		//panelTable.add(patientTable.getTableHeader(), BorderLayout.PAGE_START);
		//panelTable.add(patientTable, BorderLayout.CENTER);
		//panelTabela.add(sp);

		JPanel mainPanel = new JPanel();
		JPanel contenerLeft = new JPanel();
		contenerLeft.add(panelPatient);
		contenerLeft.add(panelExam);
		contenerLeft.setLayout(new BoxLayout(contenerLeft, BoxLayout.PAGE_AXIS));

		JPanel contenerRight = new JPanel();
		contenerRight.add(panelTable);
		contenerRight.setLayout(new BoxLayout(contenerRight, BoxLayout.PAGE_AXIS));
		// TODO    dodac contenerRight.add(patientTable);

		mainPanel.add(contenerLeft);
		mainPanel.add(contenerRight);
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.LINE_AXIS));
		frame.setContentPane(mainPanel);
		frame.pack();

	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	// /////////////    FUNKCJA CZYSZCZENIA DANYCH ogolnie/////////////////////////
	 private  void clearFields(JPanel panel){

		 Component[] components = panel.getComponents();
		 for (Component component : components) {
			 if (component instanceof JPanel) clearFields((JPanel) component);
			 if(component instanceof JTextField)((JTextField) component).setText("");
			 if (component instanceof JComboBox)((JComboBox) component).setSelectedIndex(0);
			 }
//
		 }

	 /////////////   //Blokowanie edycji paneli on/off/////////////////////////


	   public static void setPanelEditable(JPanel panel, Boolean isEditable){
	       panel.setEnabled(isEditable);

	       Component[] components = panel.getComponents();

	       for (Component component : components) {
	           if (component instanceof JPanel) {
	               setPanelEditable((JPanel) component, isEditable); //going inside the panel if needed
	           }

	           component.setEnabled(isEditable);
	       }
	   }

}
