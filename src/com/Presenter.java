package com;

import java.awt.Color;
import java.awt.Component;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import klasy.Exam;
import klasy.Patient;

import org.apache.commons.lang3.StringUtils;
import org.jdatepicker.impl.JDatePickerImpl;

public class Presenter {
	public static String[] tableColumns = {"Imi� i nazwisko", "p�e�", "PESEL",
			"ubezpieczenie", "wynik"};

	public Vector<Patient> vectorPatient = new Vector<>();



	private static DefaultTableModel createTableModel(Object [][] data) {
		 return new DefaultTableModel(data, tableColumns){
			private static final long serialVersionUID = 1L;

				@Override
		        public boolean isCellEditable(int row, int column) {
		            return false;
		        }
		    };
	}
	private static void updateTable(Vector<Patient> vectorPatient, JTable patientTable) {
		Object[][] data = new Object[vectorPatient.size()][5];
		for(int i = 0; i < vectorPatient.size();i++) data[i] = vectorPatient.get(i).getArray();// dodajemy do tabeli konkretny rzad z vectora, czyli konkretn� pozycje wektora
			patientTable.setModel(Presenter.createTableModel(data));
			setTableAlignment(patientTable, SwingConstants.CENTER);
			adjustColumns(patientTable);


	}

	public int saveOrEditPatient(String name, String surname, String pesel, boolean sex, String insurance, JTable table ) {
		Patient patient = new Patient(name, surname, pesel, sex, insurance);

		int check = checkInputValues(pesel);
		//if (check==0 || check == 3){
		if(table.getSelectionModel().isSelectionEmpty() && check == 0) {
		vectorPatient.add(patient);
		updateTable(vectorPatient, table);
		ListSelectionModel selectionModel =
                table.getSelectionModel();
        selectionModel.setSelectionInterval(vectorPatient.size()-1, vectorPatient.size()-1);
			return check;
		}
		else if (!table.getSelectionModel().isSelectionEmpty() && (check == 0 || check == 3)){
			int selectedRow = table.getSelectedRow();
			vectorPatient.setElementAt(patient, table.getSelectedRow());
			updateTable(vectorPatient, table);
			ListSelectionModel selectionModel =
	                table.getSelectionModel();
	        selectionModel.setSelectionInterval(selectedRow, selectedRow);
			return 0;
		}

		else {
			return check;
		}
	}

	public int saveExam(Date date, String conErytrocythes, String conHemoglobin, String conIron, JTable table, String stringDate){
		int check = errorInExam(conErytrocythes, conHemoglobin, conIron, stringDate);
		if(check== 0) {
			vectorPatient.get(table.getSelectedRow()).setExamResults(date, Integer.parseInt(conErytrocythes),
					Double.parseDouble(conHemoglobin),  Double.parseDouble(conIron), stringDate);		// z tej tabelki pobieramy zaznaczony rzad czyli pacjent ktorego edytujemy
			updateTable(vectorPatient, table);

		}
			return check;
	}

	public boolean checkExamNormHemoglobin(JTable table) {
		String sex = vectorPatient.get(table.getSelectedRow()).getSex();
		double con = vectorPatient.get(table.getSelectedRow()).getExamResults().getConHemoglobin();
		if(sex.equals("K") && (con<11.5 || con>16)) {
			return false;
		}
		else if(sex.equals("M") && (con<12.5 || con>18)) {
			return false;
		}
		
		return true;
	}
	public boolean checkExamNormErytrocythes(JTable table) {
		String sex = vectorPatient.get(table.getSelectedRow()).getSex();
		double con = vectorPatient.get(table.getSelectedRow()).getExamResults().getConErytrocythes();
		if(sex.equals("K") && (con<4.2 || con>5.4)) {
			return false;
		}
		else if(sex.equals("M") && (con<4.7 || con>6.1)) {
			return false;
		}
		
		return true;
	}
	public boolean checkExamNormIron(JTable table) {
		String sex = vectorPatient.get(table.getSelectedRow()).getSex();
		double con = vectorPatient.get(table.getSelectedRow()).getExamResults().getConIron();
		if(sex.equals("K") && (con<6.6 || con>26)) {
			return false;
		}
		else if(sex.equals("M") && (con<10.6 || con>28.3)) {
			return false;
		}
		
		return true;
	}


	public void deletePatient(JTable table){
		if (table.getSelectedRow()!=-1) {
			vectorPatient.remove(table.getSelectedRow());
			updateTable(vectorPatient, table);
		}
	}

	public void showPatient(JTable table, JTextField tfName, JTextField tfSurname, JTextField tfPesel, JRadioButton radMan, JRadioButton radWoman,
							JComboBox comboBox ){
		tfName.setText(vectorPatient.get(table.getSelectedRow()).getName());
		tfSurname.setText(vectorPatient.get(table.getSelectedRow()).getSurname());
		tfPesel.setText(vectorPatient.get(table.getSelectedRow()).getPesel());
			if (vectorPatient.get(table.getSelectedRow()).getSex().equals("M")) radMan.setSelected(true);
			else radWoman.setSelected(true);
		comboBox.setSelectedIndex(vectorPatient.get(table.getSelectedRow()).whichInsurance());
	}

	public void showExam(JTable table, JDatePickerImpl datePicker, JTextField tfErytro, JTextField tfHemo, JTextField tfIron){
		boolean hemoNorm =  checkExamNormHemoglobin(table);
		boolean erytroNorm = checkExamNormErytrocythes(table);
		boolean ironNorm =  checkExamNormIron(table);
		
		tfErytro.setText(String.valueOf(vectorPatient.get(table.getSelectedRow()).getExamResults().getConErytrocythes()));
		tfHemo.setText(String.valueOf(vectorPatient.get(table.getSelectedRow()).getExamResults().getConHemoglobin()));
		tfIron.setText(String.valueOf(vectorPatient.get(table.getSelectedRow()).getExamResults().getConIron()));
		if(!hemoNorm)
			tfHemo.setBackground(Color.RED);
		else
			tfHemo.setBackground(Color.GREEN);
		if(!erytroNorm)
			tfErytro.setBackground(Color.RED);
		else
			tfErytro.setBackground(Color.GREEN);
		if(!ironNorm)
			tfIron.setBackground(Color.RED);
		else
			tfIron.setBackground(Color.GREEN);
		
		datePicker.getJFormattedTextField().setText(vectorPatient.get(table.getSelectedRow()).getExamResults().getStringDate());
		LocalDate localDate = vectorPatient.get(table.getSelectedRow()).getExamResults().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year  = localDate.getYear();
		int month = localDate.getMonthValue();
		int day   = localDate.getDayOfMonth();
		datePicker.getModel().setDate(year, month-1, day);
		datePicker.getModel().setSelected(true);
	}

	
// sprawdzanie bledu przy zapisie badania
// 1 - kt�res pole jest puste
// 2 s� uzupe�nione, ale nie wszystkie s� numerami
	private int errorInExam(String conErytrocythes, String conHemoglobin, String conIron, String stringDate) {
		if(conErytrocythes.equals("") || conHemoglobin.equals("") || conIron.equals("") || stringDate.equals("")) {
			return 1;
		}
		else if(!isNumber(conErytrocythes) || !isNumber(conHemoglobin) || !isNumber(conIron)) {
			return 2;
		}
		else return 0;
	}

	// --------------------------------------------------------------------------------------------------------
	//------------------ CHECKING IF INPUT VALUES ARE CORRECT --------------------------------------------------

	public int checkInputValues(String pesel){
		if (!StringUtils.isNumeric(pesel)) return 1;
		if (pesel.length() != 11) return 2;

		//for (int i=0; i< vectorPatient.size()-1; i++) if (vectorPatient.get(i).getPesel().equals(pesel)) return 3;
		for (Patient aPatient : vectorPatient) {if (aPatient.getPesel().equals(pesel)) return  3;} //pętla for each

		return 0;	// jesli nie wystapil zaden blad to funkcja dojdzie do tego moomentu i zwracamy 0
	}


	private static void setTableAlignment(JTable table, int alignment)
    {
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(alignment);

        TableModel tableModel = table.getModel();

        for (int columnIndex = 0; columnIndex < (tableModel.getColumnCount()-1); columnIndex++)
        {
            table.getColumnModel().getColumn(columnIndex).setCellRenderer(rightRenderer);
        }
    }
		// kolumny same zmieniaja szerokosc w zaleznosci od tekstu - rozwi�zanie znalezione w internecie
	  private static void adjustColumns(JTable table){

	        for (int column = 0; column < table.getColumnCount(); column++)
	        {
	            TableColumn tableColumn = table.getColumnModel().getColumn(column);
	            int preferredWidth = tableColumn.getMinWidth();
	            int maxWidth = tableColumn.getMaxWidth();

	            for (int row = 0; row < table.getRowCount(); row++)
	            {
	                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
	                Component c = table.prepareRenderer(cellRenderer, row, column);
	                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
	                preferredWidth = Math.max(preferredWidth, width);

	                //  We've exceeded the maximum width, no need to check other rows

	                if (preferredWidth >= maxWidth)
	                {
	                    preferredWidth = maxWidth;
	                    break;
	                }
	            }

	            tableColumn.setPreferredWidth( preferredWidth );
	        }
	    }

		 public static boolean isNumber(String string){
		        boolean numeric;

		        numeric = string.matches("\\d+(\\.\\d+)?"); // \\d - ilekolwiek cyfr + (moze byc kropka i ilekolwiek cyfr)
		        return numeric;
		    }
		}
