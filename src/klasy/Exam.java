package klasy;

import java.util.Date;

public class Exam {
	
	private Date  date;
	private double conErytrocythes;
	private double conHemoglobin;
	private double conIron;
	private String stringDate;

	public Exam(Date date, double conErytrocythes, double conHemoglobin, double conIron, String stringDate) {
		this.date = date;
		this.conErytrocythes = conErytrocythes;
		this.conHemoglobin = conHemoglobin;
		this.conIron = conIron;
		this.stringDate = stringDate;
	}

	public String getStringDate() {
		return stringDate;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getConErytrocythes() {
		return conErytrocythes;
	}

	public void setConErytrocythes(double conErytrocythes) {
		this.conErytrocythes = conErytrocythes;
	}

	public double getConHemoglobin() {
		return conHemoglobin;
	}

	public void setConHemoglobin(double conHemoglobin) {
		this.conHemoglobin = conHemoglobin;
	}

	public double getConIron() {
		return conIron;
	}

	public void setConIron(double conIron) {
		this.conIron = conIron;
	}


}
