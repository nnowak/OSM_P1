package klasy;

import java.util.Date;

public class Patient  {
	private String name;
	private String surname;
	private String pesel;
	private String sex; // true - kobieta, false - mezczyzna
	private String insurance;
	private boolean exam;
	private Exam examResults;
	
	
	public Patient(String name, String surname, String pesel, boolean sex, String insurance) {
		this.name = name;
		this.surname = surname;
		this.pesel = pesel;
		if(sex)this.sex = "M"; else this.sex = "K";
		this.insurance = insurance; 
		this.exam = exam;
		
	}

	



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public boolean isExam() {
		return exam;
	}

	public void setExam(boolean exam) {
		this.exam = exam;
	}
	// object - oboj?tnie jaki obiekt mog? wprowadzi?;
	public Object[] getArray() {
		return new Object[] {getName() + " " + getSurname(), getSex(), getPesel(), getInsurance(), isExam() };
	}
	// tworzenie nowego badania
	public void setExamResults(Date date, double conErytrocythes, double conHemoglobin, double conIron, String stringDate) {
		examResults = new Exam(date, conErytrocythes, conHemoglobin, conIron, stringDate );
		this.exam = true;
	}

	public int whichInsurance(){
		int index;
		if (insurance.equals("Brak") ) index = 0;
		else if (insurance.equals("NFZ")) index = 1;
		else index = 2;
		return index;
	}

	public Exam getExamResults() {
		return examResults;
	}
}
